/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f4xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */

/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define STICK_MAX 127
#define PWM_MAX 8000
#define ESC_NOTHROTTLE 500
#define ESC_MAXTHROTTLE 1000
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */
volatile uint16_t motorL_duty;
volatile uint16_t motorR_duty;
volatile int32_t motorR_effort;
volatile int32_t motorL_effort;
volatile int16_t vel_x = 0;
volatile int16_t ang_z = 0;
volatile int16_t blade = 0;
volatile uint16_t adcRaw;

volatile uint32_t uwFrequency[2];
volatile uint32_t uwDiffCapture[2];
volatile uint32_t uwIC2Value2[2];
volatile uint32_t uwIC2Value1[2];
volatile uint32_t uhCaptureIndex[2];

extern TIM_HandleTypeDef htim2;
extern ADC_HandleTypeDef hadc1;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M4 Processor Interruption and Exception Handlers          */ 
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Pre-fetch fault, memory access fault.
  */
void BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVCall_IRQn 0 */

  /* USER CODE END SVCall_IRQn 0 */
  /* USER CODE BEGIN SVCall_IRQn 1 */

  /* USER CODE END SVCall_IRQn 1 */
}

/**
  * @brief This function handles Debug monitor.
  */
void DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */
	if(uwDiffCapture[0] < 800)
	{
		uwDiffCapture[0]-=254;
		if(uwDiffCapture[0] > 0 && uwDiffCapture[0] < 500)
			blade = uwDiffCapture[0];
	}
	if(uwDiffCapture[1] < 4800 && uwDiffCapture[1] > 5100)
	{
		HAL_GPIO_WritePin(LED2_GPIO_Port,LED2_Pin, GPIO_PIN_SET);
	}
	else if (uwDiffCapture[1] < 4600 || uwDiffCapture[1] > 5400)
	{
		HAL_GPIO_WritePin(LED2_GPIO_Port,LED2_Pin, GPIO_PIN_SET);
	}

	if(vel_x > STICK_MAX || vel_x < -STICK_MAX)
		vel_x = 0;
	if(ang_z > STICK_MAX || ang_z < -STICK_MAX)
			ang_z = 0;

	if (vel_x < 10 && vel_x > -10)
	{
		motorR_effort = +ang_z;
		motorL_effort = -ang_z;
	}
	else if (vel_x > 0)
	{
		motorR_effort = vel_x + (ang_z / 4);
		motorL_effort = vel_x - (ang_z / 4);
	}
	else
	{
		motorR_effort = vel_x - (ang_z / 4);
		motorL_effort = vel_x + (ang_z / 4);
	}


	if(motorR_effort >= 0)
	{
		HAL_GPIO_WritePin(M1_DIR1_GPIO_Port, M1_DIR1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(M1_DIR2_GPIO_Port, M1_DIR2_Pin, GPIO_PIN_SET);
		motorR_duty = motorR_effort * PWM_MAX / STICK_MAX;
	}
	else
	{
		HAL_GPIO_WritePin(M1_DIR1_GPIO_Port, M1_DIR1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(M1_DIR2_GPIO_Port, M1_DIR2_Pin, GPIO_PIN_RESET);
		motorR_duty = -motorR_effort * PWM_MAX / STICK_MAX;
	}
	if(motorL_effort >= 0)
	{
		HAL_GPIO_WritePin(M2_DIR1_GPIO_Port, M2_DIR1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(M2_DIR2_GPIO_Port, M2_DIR2_Pin, GPIO_PIN_RESET);
		motorL_duty = motorL_effort * PWM_MAX / STICK_MAX;
	}
	else
	{
		HAL_GPIO_WritePin(M2_DIR1_GPIO_Port, M2_DIR1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(M2_DIR2_GPIO_Port, M2_DIR2_Pin, GPIO_PIN_SET);
		motorL_duty = -motorL_effort * PWM_MAX / STICK_MAX;
	}
	TIM1->CCR1 = motorL_duty;
	TIM1->CCR2 = motorR_duty;
	if(blade + ESC_NOTHROTTLE < ESC_MAXTHROTTLE)
		TIM4->CCR1 = blade + ESC_NOTHROTTLE;
	else
		TIM4->CCR1 = ESC_MAXTHROTTLE;
  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles TIM2 global interrupt.
  */
void TIM2_IRQHandler(void)
{
  /* USER CODE BEGIN TIM2_IRQn 0 */

  /* USER CODE END TIM2_IRQn 0 */
  HAL_TIM_IRQHandler(&htim2);
  /* USER CODE BEGIN TIM2_IRQn 1 */

  /* USER CODE END TIM2_IRQn 1 */
}

/**
  * @brief This function handles TIM3 global interrupt.
  */
void TIM3_IRQHandler(void)
{
  /* USER CODE BEGIN TIM3_IRQn 0 */

  /* USER CODE END TIM3_IRQn 0 */
  HAL_TIM_IRQHandler(&htim3);
  /* USER CODE BEGIN TIM3_IRQn 1 */

  /* USER CODE END TIM3_IRQn 1 */
}

/* USER CODE BEGIN 1 */
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
  if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_4 && htim->Instance == TIM3)
  {
    if(uhCaptureIndex[0] == 0)
    {
      /* Get the 1st Input Capture value */
      uwIC2Value1[0] = TIM3->CCR4;
      uhCaptureIndex[0] = 1;
    }
    else if(uhCaptureIndex[0] == 1)
    {
      /* Get the 2nd Input Capture value */
      uwIC2Value2[0] = HAL_TIM_ReadCapturedValue(&htim3, TIM_CHANNEL_4);

      /* Capture computation */
      if (uwIC2Value2[0] > uwIC2Value1[0])
      {
        uwDiffCapture[0] = (uwIC2Value2[0]- uwIC2Value1[0]);
      }
      else if (uwIC2Value2[0] < uwIC2Value1[0])
      {
        /* 0xFFFF is max TIM3_CCRx value */
        uwDiffCapture[0] = ((9999 - uwIC2Value1[0]) + uwIC2Value2[0]) + 1;
      }
      /* Frequency computation: for this example TIMx (TIM3) is clocked by
         2xAPB1Clk */
      uwFrequency[0] = (2*HAL_RCC_GetPCLK1Freq()) / uwDiffCapture[0];
      uhCaptureIndex[0] = 0;
    }
  }
  if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_4 && htim->Instance == TIM2)
    {
      if(uhCaptureIndex[1] == 0)
      {
        /* Get the 1st Input Capture value */
        uwIC2Value1[1] = TIM2->CCR4;
        uhCaptureIndex[1] = 1;
      }
      else if(uhCaptureIndex[1] == 1)
      {
        /* Get the 2nd Input Capture value */
        uwIC2Value2[1] = HAL_TIM_ReadCapturedValue(&htim2, TIM_CHANNEL_4);

        /* Capture computation */
        if (uwIC2Value2[1] > uwIC2Value1[1])
        {
          uwDiffCapture[1] = (uwIC2Value2[1]- uwIC2Value1[1]);
        }
        else if (uwIC2Value2[1] < uwIC2Value1[1])
        {
          /* 0xFFFF is max TIM3_CCRx value */
          uwDiffCapture[1] = ((9999 - uwIC2Value1[1]) + uwIC2Value2[1]) + 1;
        }
        /* Frequency computation: for this example TIMx (TIM3) is clocked by
           2xAPB1Clk */
        uwFrequency[1] = (2*HAL_RCC_GetPCLK1Freq()) / uwDiffCapture[1];
        uhCaptureIndex[1] = 0;
      }
    }
}
/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
